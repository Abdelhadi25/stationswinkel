<title>StationsWinkel</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="images/favicon.png" rel="icon"/>
<link href="css/magnific-popup.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<script src= "js/jquery.min.js" type= "text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/magnific-popup.js" type="text/javascript"></script>
<script src="js/jquery.imagesloaded.js" type="text/javascript"></script>
<script src="js/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="js/shuffle.min.js" type="text/javascript"></script>
<script src="js/shuffle.custom.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
