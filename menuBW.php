
<!DOCTYPE html>
<html>
    <head>
        <?php include 'head.php'?>
    </head>
    <body id="menutab">
<!--    <body id="menutab" data-spy="scroll" data-target=".navbar" data-offset="50">-->

        <!--Navigation Section-->
        <div>
            <?php include 'navbar.php'?>
        </div>
        <!--End Navigation Section-->
        
        <!--DropDown Script-->
        <script>
            $(document).ready(function () {
                $('.Navigation-listItem').click(function (e) {
                    if ($(this).children('.Navigation-list.is-dropdown').hasClass('is-hidden')) {
                        $(".Navigation-list.is-dropdown").addClass('is-hidden');
                        $(this).children('.Navigation-list.is-dropdown').removeClass('is-hidden');
                    } else {
                        $(".Navigation-list.is-dropdown").addClass('is-hidden');
                    }
                });
                $('.Navigation-listItem .Navigation-list.is-dropdown').click(function (e) {
                    e.stopPropagation();
                });
            });

            $(document).click(function () {
                $(".Navigation-list.is-dropdown").addClass('is-hidden');
            });

            $(".Navigation-listItem.is-dropdown").click(function (e) {
                e.stopPropagation();
            });
        </script>
        <!--End DropDown Script-->

<!--Banner Section-->
<div class="tv-banner-image" style="background: rgba(0, 0, 0, 0) url('images/Image24.jpeg') no-repeat scroll center top / cover;">
    <div class="tv-banner-title">
        <h1>Welkom bij Boxmeer Winkel </h1>
        <p>Kijk het menu van de andere locaties</p>
        <a href="menuDW.php">Dukenburg Winkel</a>
        <a href="menu.php">Boxmeer Cafetaria</a>
    </div>
</div>
<!--End Banner Section-->

        <!--Menu Tab Section-->
<section id="innermenutab" class="tv-section-padding">
    <div class="container">
        <div class="row text-center">
            <div class="tv-section-title text-center">
                <h2>Grid Tab</h2>
                <img src="images/mustache-shape(2).png">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tv-gallery-menu">
                    <!--Nav tabs-->
                    <ul class="nav nav-tabs tabs-left home-tabs">
                        <li class="active"><a class="tab-menu" data-toggle="tab" href="#broodje" aria-expanded="true"><span>Broodjes</span></a></li>
                        <li class=""><a class="tab-menu" data-toggle="tab" href="#dranken" aria-expanded="true"><span>Dranken</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="tab-content tv-menu-tab">
                        <div id="broodje" class="tab-pane active" data-groups="[&quot;broodje&quot;]">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="tv-menu-block">
                                        <div class="menu-title">
                                        <span>Broodjes</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje1.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje2.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje3.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje4.jpeg">
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Broodje döner - €5,00</a>
                                                <p>Salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Döner menu - €7,50</a>
                                                <p>Friet, blikje fries, salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Dürum Döner - €5,00</a>
                                                <p>Salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Dürum Döner menu - €7,50</a>
                                                <p>Friet, blikje fries, salade en saus</p>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje5.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje6.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje7.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje8.jpeg">
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Turkse pizza - €3,00</a>
                                                <p>Pikante gehakt, salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Turkse pizza menu - €5,50</a>
                                                <p>Friet, blikje fries, salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Kapsalon - €7,00</a>
                                                <p>Super 7,00€</p>
                                                <p>Normaal 5,50€</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Döner box - €5,00</a>
                                                <p>Kipdöner, salade en saus</p>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/broodje9.jpeg">
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Kinder menu - €4,95</a>
                                                <p>Kleine friet met kipcorn, Kipnuggets of frikandel, Drankje en een VERRASING!</p>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dranken" class="item tab-pane" data-groups="[&quot;Dranken&quot;]">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tv-menu-block">
                                    <div class="menu-title">
                                        <span>Dranken</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank1.jpg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank2.jpg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank3.jpg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank4.jpeg">
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Blikje fries - €1,80</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Petfles - €2,30</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Chaudfontaine water €2,00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Spa water €2,00</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank5.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank6.jpg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank7.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank7.jpeg">
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Bier groot blikje €2,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Bier kleine blikje €2,00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Red bull klein €2,40</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Red bull groot €3,00</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank8.jpeg">
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left; height: 230px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 100%;">
                                                <img class="img100" src="images/drank9.jpeg">
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Hawai €1,80</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 " style="text-align: left">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                <a href="blog_single_post_with_rightsidebar.html">Ayran € 1,50</a>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<!--End Menu Tab Section-->



        <!--Footer Section-->
<footer id="contactus" class="tv-section-padding">
    <?php include 'foot.php'?>
</footer>
        <!--End Footer Section-->

        <a id="back-to-top" style="display: none;"><i class="fa fa-caret-up fa-lg"></i></a>
    </body>
</html>
