<nav class="navbar navbar-default navbar-fixed-top" style="height: 70px;">
    <div class="container" style="height: 100%;">
        <div class="row" style="height: 100%;">
            <div class="col-md-2 col-sm-3 col-xs-12" style="height: 100%;">
                <div class="navbar-header text-center" style="height: 100%;" >
                    <a href="home.php" class="navbar-brand" style="padding: 0px; height: 100%"><img src="images/favicon.png" style="width:80%; height: 100%; "></a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#tv-navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-xs-6" style="height: 100%;">

                <div class="collapse navbar-collapse" id="tv-navbar" style="height: 100% !important;">
                    <ul class="nav navbar-nav text-center main-menu" style="height: 100% !important;">

                        <li class="" style="margin-top: 10px"><a href="home.php" class="tv-menu">Home</a></li>


                        <li class="tv-drop-menu">
                            <a data-toggle="dropdown" aria-expanded="false" class="tv-menu">menu<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu tv-sub-menu">
                                <li class=""><a href="menu.php" class="tv-menu">menu BoxmmerCafetaria</a></li>
                                <li class=""><a href="menuBW.php" class="tv-menu">menu BoxmmerWinkel</a></li>
                                <li class=""><a href="menuDW.php" class="tv-menu">menu DukenburgWinkel</a></li>

                            </ul>
                        </li>

                        <li class="tv-drop-menu">
                            <a data-toggle="dropdown" aria-expanded="false" class="tv-menu">Contact us<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu tv-sub-menu">
                                <li class=""><a href="contactBC.php" class="tv-menu">Contact met BoxmmerCafetaria</a></li>
                                <li class=""><a href="contactBW.php" class="tv-menu">contact met BoxmmerWinkel</a></li>
                                <li class=""><a href="contactDW.php" class="tv-menu">contact met DukenburgWinkel</a></li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>