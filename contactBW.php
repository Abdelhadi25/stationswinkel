<?php
session_start();
include 'connection.php';
$query = 'Select * from opentijden_bw';
$sth = $conn->prepare($query);
$sth->execute();
$result = $sth->fetchAll();

$contact = 'Select nummer from contact_bw';
$sth = $conn->prepare($contact);
$sth->execute();
$number = $sth->fetch(PDO::FETCH_ASSOC);
?>
<html>
    <head>
        <?php include 'head.php'?>
    </head>
    <body id="blogsingleleftsidebar">

        <!--Navigation Section-->
            <div>
                <?php include 'navbar.php' ?>
            </div>
        <!--End Navigation Section-->
        
        <!--DropDown Script-->
        <script>
            $(document).ready(function () {
                $('.Navigation-listItem').click(function (e) {
                    if ($(this).children('.Navigation-list.is-dropdown').hasClass('is-hidden')) {
                        $(".Navigation-list.is-dropdown").addClass('is-hidden');
                        $(this).children('.Navigation-list.is-dropdown').removeClass('is-hidden');
                    } else {
                        $(".Navigation-list.is-dropdown").addClass('is-hidden');
                    }
                });
                $('.Navigation-listItem .Navigation-list.is-dropdown').click(function (e) {
                    e.stopPropagation();
                });
            });

            $(document).click(function () {
                $(".Navigation-list.is-dropdown").addClass('is-hidden');
            });

            $(".Navigation-listItem.is-dropdown").click(function (e) {
                e.stopPropagation();
            });
        </script>
        <!--End DropDown Script-->

        <!--Banner Section-->
        <div class="tv-banner-image tv-blog-banner-img" style="background: rgba(0, 0, 0, 0) url('images/Image24.jpeg') no-repeat scroll center top / cover;">
            <div class="tv-banner-title">
                <h1>Contact Us</h1>
            </div>
        </div>
        <!--End Banner Section-->

        <!--Contact Us Section-->
        <section id="contact-us" class="tv-section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="contact-block-color">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div id="map" class="ct-map-display"></div>
                                    <div class="contact-us-box">
                                        <div class="row">

                                            <div class="col-md-6 col-sm-6 col-xs-12 contact-box-area mrb-dw-20">
                                                <h3 class="contact-title fs-dw-20 mrb-dw-15 mrb-up-10">
                                                    Openingstijden
                                                </h3>
                                                <div class="ct-opening-hours">
                                                    <?php
                                                    foreach ($result as $value){?>
                                                        <p><strong><?=$value["Dag"]?>.</strong><?=$value['openingstijd']?></p>
                                                    <?}
                                                    ?>
                                                </div>
                                        </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 contact-box-area mrb-dw-20">
                                                <h3 class="contact-title fs-dw-20 mrb-dw-15 mrb-up-10">
                                                    Neem contact met ons!
                                            </h3>
                                            <div class="ct-opening-hours">
                                                <p><strong>Telefoonnummer:</strong><?= $number['nummer']?></p><br>
                                                <h3 class="contact-title fs-dw-20 mrb-dw-15 mrb-up-10">
                                                    Of kom maar langs!
                                                </h3>
                                                <div class="ct-opening-hours">
                                                    <p><strong>Adres:</strong> Zwanenveld 8401</p>
                                                    <p><strong>Postcode:</strong>6538 TL Nijmegen</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Contact Us Section-->


        <!--Footer Section-->
        <footer id="contactus" class="tv-section-padding">
            <?php include 'foot.php'?>
        </footer>
        <!--End Footer Section-->

        <!--Back To Top Code-->
        <a id="back-to-top" style="display: none;"><i class="fa fa-caret-up fa-lg"></i></a>
        <!--End Back To Top Code-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIO0h0OU2x-1Y88cukZ0YB2GhjGuo2Giw"></script>
        <script src="js/map-customBW.js"></script>
    </body>
</html>
