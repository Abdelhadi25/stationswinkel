<?php
$servername = "localhost";
$username = "root";
$password = "root";

/** @var PDO $conn */
try {
    $conn = new PDO("mysql:host=$servername;dbname=station_winkel", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
    die();
}

