<?php
//session_start();
//include 'connection.php';
//$query = 'Select * from actie';
//$sth = $conn->prepare($query);
//$sth->execute();
//$result = $sth->fetchAll();
//?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php'?>
</head>

<body id="menutab">
<!--    <body id="menutab" data-spy="scroll" data-target=".navbar" data-offset="50">-->

<!--Navigation Section-->
<div>
    <?php include 'navbar.php'?>
</div>
<!--End Navigation Section-->

<!--DropDown Script-->
<script>
    $(document).ready(function() {
        $('.Navigation-listItem').click(function(e) {
            if ($(this).children('.Navigation-list.is-dropdown').hasClass('is-hidden')) {
                $(".Navigation-list.is-dropdown").addClass('is-hidden');
                $(this).children('.Navigation-list.is-dropdown').removeClass('is-hidden');
            } else {
                $(".Navigation-list.is-dropdown").addClass('is-hidden');
            }
        });
        $('.Navigation-listItem .Navigation-list.is-dropdown').click(function(e) {
            e.stopPropagation();
        });
    });

    $(document).click(function() {
        $(".Navigation-list.is-dropdown").addClass('is-hidden');
    });

    $(".Navigation-listItem.is-dropdown").click(function(e) {
        e.stopPropagation();
    });
</script>
<!--End DropDown Script-->

<!--Banner Section-->
<div class="tv-banner-image" style="background: rgba(0, 0, 0, 0) url('images/Image24.jpeg') no-repeat scroll center top / cover;">
    <div class="tv-banner-title">
        <h1>Welkom bij Boxmeer Cafetaria</h1>
        <p>Kijk het menu van de andere locaties</p>
        <a href="menuDW.php">Dukenburg winkel</a>
        <a href="menuBW.php">Boxmeer winkel</a>
    </div>
</div>
<!--End Banner Section-->

<!--Menu Tab Section-->
<section id="innermenutab" class="tv-section-padding">
    <div class="container">
        <div class="row text-center">
            <div class="tv-section-title text-center">
                <h2>Grid Tab</h2>
                <img src="images/mustache-shape(2).png">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tv-gallery-menu">
                    <!--Nav tabs-->
                    <ul class="nav nav-tabs tabs-left home-tabs">
                        <li class="active"><a class="tab-menu" data-toggle="tab" href="#snacks" aria-expanded="false"><span>Snacks</span></a></li>
                        <li class=""><a class="tab-menu" data-toggle="tab" href="#friet" aria-expanded="false"><span>Friet</span></a></li>
                        <li class=""><a class="tab-menu" data-toggle="tab" href="#combi" aria-expanded="false"><span>Combi menu</span></a></li>
                        <li class=""><a class="tab-menu" data-toggle="tab" href="#broodje" aria-expanded="true"><span>Broodjes</span></a></li>
                        <li class=""><a class="tab-menu" data-toggle="tab" href="#dessert" aria-expanded="true"><span>Dessert</span></a></li>
                        <li class=""><a class="tab-menu" data-toggle="tab" href="#dranken" aria-expanded="true"><span>Dranken</span></a></li>
                        <li class=""><a class="tab-menu" data-toggle="tab" href="#akties" aria-expanded="true"><span>Akties</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="tab-content tv-menu-tab">
                        <div id="snacks" class="tab-pane active" data-groups="[&quot;snacks&quot;]">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tv-menu-block">
                                    <div class="menu-title">
                                        <span>Snacks</span>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/frikandel2.jpg">
                                                <a href="">Frikandel - €1,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/kroket.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Kroket - €1,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/rundkroket.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Rundkroket - €1,80</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/goulashkroket.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Goulashkroket - €1,80</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/satekroket.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Satékroket - €1,80</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/kaassouffle.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Kaassoufflé - €1,60</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/kipcorn.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Kipcorn - €2,00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/kipnuggats.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Kipnuggets - €2,00</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/mexicano.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Mexicano - €2,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/mexicanobaguette.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Mexicano baguette - €3,75</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/mexicanowrap.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Mexicano wrap - €3,75</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/sitostick.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Sito stick - $2,50</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/berehap.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Berehap - €2,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/viandel.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Viandel - €2,20</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/bamiblok.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Bamiblok/ Bamoschijf - €1,80</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/nasischijf.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Nasischijf - €1,80</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/anonimo.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Anonimo - €2,20  </a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/gehaktstaaf.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Gehaktstaaf - €2,20</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/bitterballen.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Bitterballen - €2,00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/kipburger.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Kipburger - 3,50</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/realburger.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Realburger - €3,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/bigamerican.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Big American - €3,75</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/broodje.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Broodje - €0,60</a>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="friet" class="tab-pane" data-groups="[&quot;friet&quot;]">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tv-menu-block">
                                    <div class="menu-title">
                                        <span>FRIET</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/frietklein.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Friet klein - €1,80</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/frietklein.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Friet groot - €2,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/frietpuntzak.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Friet puntzak - €1,70</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/saus.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Mayo, Ketchup, Curry - €0,50</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/Frietspeciaal.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Speciaal, Saté, Joppie - €0,80</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/stoofvlees.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Stoofvlees - €1,65</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/gezinzak.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Gezinzak friet 2 pers. - €3,40</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/gezinzak.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Gezinzak friet 3 pers. - €5,00</a>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/gezinzak.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Gezinzak friet 4 pers. - €6,50</a>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="combi" class="tab-pane" data-groups="[&quot;combi&quot;]">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tv-menu-block">
                                    <div class="menu-title">
                                        <span>COMBI MENU</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 65%;">
                                                <img class="img100" src="images/combi1.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">1 Broodje kipburger/ Friet/ Friesdrank - €6,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 65%;">
                                                <img class="img100" src="images/combi2.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">1 Broodje habmurger/ Friet/ Friesdrank - €6,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 65%;">
                                                <img class="img100" src="images/combi3.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">1 Kaassouffié/ Bamiblok/ Friet/ Friesdrank - €6,00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 65%;">
                                                <img class="img100" src="images/combi4.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">1 Frikandel/ 1 Kroket/ Friet/ Friesdrank - €5,50</a>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="broodje" class="tab-pane" data-groups="[&quot;broodje&quot;]">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tv-menu-block">
                                    <div class="menu-title">
                                        <span>Broodjes</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje1.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Broodje döner - €5,00</a>
                                                <p>Salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje2.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Döner menu - €7,50</a>
                                                <p>Friet, blikje fries, salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje3.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Dürum Döner - €5,00</a>
                                                <p>Salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje4.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Dürum Döner menu - €7,50</a>
                                                <p>Friet, blikje fries, salade en saus</p>
                                            </li>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje5.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Turkse pizza - €3,00</a>
                                                <p>Pikante gehakt, salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje6.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Turkse pizza menu - €5,50</a>
                                                <p>Friet, blikje fries, salade en saus</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje7.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Kapsalon - €7,00</a>
                                                <p>Super 7,00€</p>
                                                <p>Normaal 5,50€</p>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje8.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Döner box - €5,00</a>
                                                <p>Kipdöner, salade en saus</p>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 70%;">
                                                <img class="img100" src="images/broodje9.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Kinder menu - €4,95</a>
                                                <p>Kleine friet met kipcorn, Kipnuggets of frikandel, Drankje en een VERRASING!</p>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dessert" class="item tab-pane" data-groups="[&quot;dessert&quot;]">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tv-menu-block">
                                    <div class="menu-title">
                                        <span>DESSERTS</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <img src="images/desserts.jpeg" style="width: 545px;">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="menus-list">
                                                <ul class="list-group">
                                                    <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                        <a href="blog_single_post_with_rightsidebar.html">IJSHOORNS</a>
                                                        <ul>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Kids cone 1€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Oublie normaal 1,60€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Oublie normaal 1,60€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Oublie groot 2,00€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Reuze beker 1,80€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">softijs beker klein 2,75€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">softijs beker normaal 3,25€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">softijs beker groot 3,75€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Ijsdips disco, choco,nootjes, smurfen 0,30€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Slagroom 0,50€</li>
                                                        </ul>
                                                    </li>
                                                    <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                        <a href="blog_single_post_with_rightsidebar.html">MILKSHAKES</a>
                                                        <ul>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Klein 2.00€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Normaal 2,50€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Oublie normaal 1,60€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Groot 3,00€</li>
                                                        </ul>
                                                    </li>
                                                    <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                        <a href="blog_single_post_with_rightsidebar.html">SHUFFLE</a>
                                                        <ul>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Stroopwafel 3.60€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Choco mini´s 3.60€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">KitKat 3.60€</li>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Oreo 3.60€</li>
                                                        </ul>
                                                    </li>
                                                    <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                        <a href="blog_single_post_with_rightsidebar.html">SUNDAES</a>
                                                        <ul>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Aardbei, Chokolade, Mango passie, Zoute drop, Toffee- caramel:</li>
                                                            <ul>
                                                                <li>Klein 1,70€</li>
                                                                <li>Normaal 2,00€</li>
                                                                <li>Groot 2,30€</li>
                                                            </ul>
                                                        </ul>
                                                    </li>
                                                    <li class="list-group-item tv-menu-content border-transparent menu-item">
                                                        <a href="blog_single_post_with_rightsidebar.html">Sorbets</a>
                                                        <ul>
                                                            <li class="lihadi" style="font-size: 20px;  list-style-type: none;">Aardbei, Kersen, Tropical, Rumrozijnen:</li>
                                                            <ul>
                                                                <li>Klein 2,60€</li>
                                                                <li>Groot 3,60€</li>
                                                            </ul>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="dranken" class="item tab-pane" data-groups="[&quot;Dranken&quot;]">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tv-menu-block">
                                    <div class="menu-title">
                                        <span>Dranken</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank1.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Blikje fries - €1,80</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank2.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Petfles - €2,30</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank3.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Chaudfontaine water €2,00</a>

                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank4.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Spa water €2,00</a>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank5.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Bier groot blikje €2,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank6.jpg">
                                                <a href="blog_single_post_with_rightsidebar.html">Bier kleine blikje €2,00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank7.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Red bull klein €2,40</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank7.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Red bull groot €3,00</a>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank9.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Ayran € 1,50</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 350px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/drank8.jpeg">
                                                <a href="blog_single_post_with_rightsidebar.html">Hawai €1,80</a>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="akties" class="item tab-pane" data-groups="[&quot;Aktie&quot;]">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tv-menu-block">
                                    <div class="menu-title">
                                        <span>Aktie</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/frikandel2.jpg">
                                                <h4>Maandag</h4>
                                                <a>2 Frikandelen - €2,00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/kaassouffle.jpg">
                                                <h4>Dinsdag</h4>
                                                <a>2 Kaassouffle - €2,00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/Bamiblok.jpg">
                                                <h4>Woensdag</h4>
                                                <a>2 Bamiblok - €2.00</a>
                                            </li>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/kipcorn.jpg">
                                                <h4>Donderdag</h4>
                                                <a>2 Kipcorn - €2,00</a>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="text-align: left; height: 450px;">
                                            <li class="list-group-item tv-menu-content border-transparent menu-item" style="width: 100%; height: 80%;">
                                                <img class="img100" src="images/kroket.jpg">
                                                <h4>Vrijdag</h4>
                                                <a>2 Kroketen - €2,00</a>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<!--End Menu Tab Section-->

<!--Footer Section-->
<footer id="contactus" class="tv-section-padding">
    <?php include 'foot.php'?>
</footer>
<!--End Footer Section-->

<a id="back-to-top" style="display: none;"><i class="fa fa-caret-up fa-lg"></i></a>
</body>

</html>