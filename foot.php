<div class="tv-section-footer-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="tv-footer-content">
                    <h3>StationsWinkel</h3>
                    <p>Welkom bij onze !</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="tv-footer-content">
                    <h3>Snel Links</h3>
                    <ul class="list-unstyled footer-menu">
                        <li class="active"><a href="home.php">Home</a></li>
                        <li class=""><a href="menu.php">Menu</a></li>
                        <li class=""><a href="contactBC.php">Contact Boxmeer Cafetaria</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="tv-footer-content">
                    <h3>Over ons!</h3>
                    <p>00000</p>
                </div>
            </div>


            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="tv-footer-content">
                    <h3>Vragen?</h3>
                    <p>Altijd bellen naar:</p>
                    <p>06284855486</p>

                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tv-footer-copyright text-center">
                    <p>&copy; Alle rechten voorbehouden voor StationWinkels 2020. </p>
                </div>
            </div>
        </div>
    </div>
</div>
