<!DOCTYPE html>
<html>
<head>
    <?php include 'head.php'?>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
<!--<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">-->

<!--Navigation Section-->
<div>
    <?php include 'navbar.php' ?>
    </nav>
</div>
<!--End Navigation Section-->

<!--Banner Section-->
<div class="tv-banner-image" style="background: rgba(0, 0, 0, 0) url('images/Image24.jpeg') no-repeat scroll center top / cover;">
    <div class="tv-banner-title">
        <h1>Welkom bij Boxmeer Cafetaria</h1>
        <p>Andere locaties</p>
        <a href="homeDW.php">Dukenburg winkel</a>
        <a href="homeBW.php">Boxmeer winkel</a>
    </div>
</div>
<!--End Banner Section-->


<!--Lunch Menu Section-->
<section id="lunch" class="tv-section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-6 col-xs-12">
                <div class="tv-menu-img">
                    <img src="images/Image4.png" class="width-100 img-responsive">
                </div>
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="tv-menu-info">
                    <h2>Lunch Menu</h2>
                    <p>Twifelt u? Dan kijk bij onze lange menu!</p>
                    <a href="menu.php">Ga naar de menu</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Lunch Menu Section-->

<!--Gallery Section-->
<div id="gallery" class=" tv-section-padding" style="">
    <div class="container">
        <div class="row">
            <div class="tv-section-title text-center">
                <h2>Our Gallery</h2>
                <img src="images/mustache-shape(2).png">
            </div>
            <div class="masonry">
                <div class="item">
                    <img src="images/8.jpg">
                </div>
                <div class="item">
                    <img src="images/17.jpg">
                </div>
                <div class="item">
                    <img src="images/18.jpg">
                </div>
                <div class="item">
                    <img src="images/realburger.jpg">
                </div>
                <div class="item">
                    <img src="images/20.jpg">
                </div>
                <div class="item">
                    <img src="images/22.jpg">
                </div>
                <div class="item">
                    <img src="images/bigamerican.jpg">
                </div>
                <div class="item">
                    <img src="images/29.jpg">
                </div>
                <div class="item">
                    <img src="images/kaassouffle.jpg">
                </div>
                <div class="item">
                    <img src="images/kroket.jpg">
                </div>
                <div class="item">
                    <img src="images/rundkroket.jpg">
                </div>
                <div class="item">
                    <img src="images/berehap.jpg">
                </div>
                <div class="item">
                    <img src="images/kipcorn.jpg">
                </div>
                <div class="item">
                    <img src="images/kipnuggats.jpg">
                </div>
            </div>
        </div>
    </div>

<!--End Gallery Section-->


<footer id="contactus" class="tv-section-padding">
    <?php include 'foot.php'?>
</footer>

<!--Scrolling Script-->
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

            if (this.hash !== "") {

                event.preventDefault();

                // Store hash
                var hash = this.hash;

                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    })
</script>
<!--End Scrolling Script-->

<!--Masonry Script-->
<script>
    $blog = $('.tv-image-masonry');
    $blog.imagesLoaded(function () {
        $('.tv-image-masonry').masonry({
            // options
            itemSelector: '.t',
            percentPosition: true,
        });
    });
</script>
<!--End Masonry Script-->

<!--Magnefic Popup Script-->
<script>
    $(document).ready(function () {
        var $imagePopup = $('[data-image-popup]');
        /*Image*/
        $imagePopup.magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    });
</script>
<!--Magnefic Popup Script Ends Here..-->

<!--DropDown Script-->
<script>
    $(document).ready(function () {
        $('.Navigation-listItem').click(function (e) {
            if ($(this).children('.Navigation-list.is-dropdown').hasClass('is-hidden')) {
                $(".Navigation-list.is-dropdown").addClass('is-hidden');
                $(this).children('.Navigation-list.is-dropdown').removeClass('is-hidden');
            } else {
                $(".Navigation-list.is-dropdown").addClass('is-hidden');
            }
        });
        $('.Navigation-listItem .Navigation-list.is-dropdown').click(function (e) {
            e.stopPropagation();
        });
    });

    $(document).click(function () {
        $(".Navigation-list.is-dropdown").addClass('is-hidden');
    });

    $(".Navigation-listItem.is-dropdown").click(function (e) {
        e.stopPropagation();
    });
</script>
<!--End DropDown Script-->

<a id="back-to-top" style="display: none;"><i class="fa fa-caret-up fa-lg"></i></a>
</body>
</html>
